from tkinter import *

n = 0
click = 1

def add():
    global n, click
    n = n + click
    label.config(text=f'{n} Euro')

def bonus():
    global click
    click += 1


window = Tk()
window.title("Cookie clicker")
window.geometry("1080x1080")
window.minsize(480, 360)
window.iconbitmap("chocolat_cookie.ico")
window.config(background="#45B39D")

frame=Frame(window,background='#34495E')
shop = Frame(window,background='#34495E')
inutil = Frame(shop,background='#344959')

cookie = PhotoImage(file="chip_cookie.png").subsample(7)
cookie.config(height=300, width=400)
button = Button(frame,image=cookie, command=add)
biscuits= PhotoImage(file="biscuits.png").subsample(2)
cookie_box = PhotoImage(file='boite-a-cookies.png').subsample(2)
shop_title = Label(shop, text='E-Shop', font=('Helvetica', 40), bg="#45B39D", fg="white")
label = Label(frame, text=f'{n} Euros', font=('Helvetica', 40), bg='#34495E', fg="white")

premier_item = Button(shop,image=cookie_box,command=bonus)
second_item = Button(shop,image=biscuits,command=bonus)
shop_title.config(pady=20,background='#34495E')

label.pack()
button.pack(side='top')
shop_title.grid(row=0,column=0)
premier_item.grid(row=2,column=0)
second_item.grid(row=2,column=1)

shop.pack(side='left')
frame.pack(side='right')

window.mainloop()
